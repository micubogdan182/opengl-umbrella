
#include "glos.h"

#include <GL/gl.h> 
#include <GL/glu.h>
#include <GL/glaux.h>
#include <math.h>

void myinit(void);
static GLfloat x = 0, y = 0, alfa = 0, i, k;
void CALLBACK display(void);
void CALLBACK myReshape(GLsizei w, GLsizei h);
void CALLBACK MutaStanga(void);
void CALLBACK MutaDreapta(void);
void CALLBACK MutaSus(void);
void CALLBACK MutaJos(void);

void CALLBACK rot_z_up(AUX_EVENTREC *event);
void CALLBACK rot_z_down(AUX_EVENTREC *event);

void CALLBACK InavarteSus(void);

GLUquadricObj *obj;


void myinit(void) {
	glClearColor(0.0, 0.0, 0.0, 0.0);

	GLfloat mat_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat mat_diffuse[] = { 0.8, 0.8, 0.8, 1.0 };
	/*  rflectanta speculara si exponentul de reflexie speculara
	nu sunt la valorile implicite (0.0)   */
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 50.0 };
	// valori implicite pentru intensitatea sursei LIGHT0
	GLfloat light_ambient[] = { 1.0, 0.0, 0.0, 1.0 };
	GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	/*  pozitia sursei nu are valori implicite */
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };

	GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 1.0 };

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	glEnable(GL_LIGHTING); // activare iluminare
	glEnable(GL_LIGHT0);	// activare sursa 0

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

}


void CALLBACK MutaStanga(void)
{
	x = x - 10;
}

void CALLBACK MutaDreapta(void)
{
	x = x + 10;
}

void CALLBACK MutaJos(void)
{
	y = y - 10;
}

void CALLBACK MutaSus(void)
{
	y = y + 10;
}

/*void CALLBACK InvarteSus(void)
{
	y = y + 10;
}*/

void CALLBACK rot_z_up(AUX_EVENTREC *event){
	alfa += 10;
}
void CALLBACK rot_z_down(AUX_EVENTREC *event){
	alfa -= 10;
}

void CALLBACK display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	glTranslatef(x, y, 0.0);
	glRotatef(90, 1, 0, 0);//sa se roteasca doar pe x

	obj = gluNewQuadric();

	gluQuadricDrawStyle(obj, GLU_FILL);

	glPushMatrix();

	//sfera
	glColor3f(255, 0, 0);
	glTranslatef(x + 10, y, 0); // style , metoda de desenare
	//glTranslatef(0, 50, 0);
	glTranslatef(0, 0, +45);
	glRotatef(alfa, 0, 1, 0);
	glTranslatef(0, 0, -45);
	//glTranslatef(x , y, 0);
	//glTranslatef(0, -50, 0);
	gluCylinder(obj, 0, 40, 20, 10, 1);
	//gluCylinder(obj, 0.2, 0.5, 65, 10, 1);
	glPopMatrix();

	glPushMatrix();

	//sfera
	glColor3f(255, 0, 0);
	glTranslatef(x + 10, y, 0); // style , metoda de desenare
	//gluCylinder(obj, 0, 40, 20, 10, 1);
	gluCylinder(obj, 0.2, 0.5, 65, 10, 1);
	glPopMatrix();

	



	glFlush();
}


void CALLBACK myReshape(GLsizei w, GLsizei h)
{
	if (!h) return;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h)
		glOrtho(-160.0, 160.0, -160.0*(GLfloat)h / (GLfloat)w,
		160.0*(GLfloat)h / (GLfloat)w, -100.0, 100.0);
	else
		glOrtho(-160.0*(GLfloat)w / (GLfloat)h,
		160.0*(GLfloat)w / (GLfloat)h, -160.0, 160.0, -100.0, 100.0);//volumul de vizualizare
	glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv)
{
	auxInitDisplayMode(AUX_SINGLE | AUX_RGB);
	auxInitPosition(0, 0, 300, 200);
	auxInitWindow("umbrela");
	myinit();
	//translatie pe axa X
	auxKeyFunc(AUX_LEFT, MutaStanga);
	auxKeyFunc(AUX_RIGHT, MutaDreapta);

	//translatie pe axa Y
	auxKeyFunc(AUX_DOWN, MutaJos);
	auxKeyFunc(AUX_UP, MutaSus);

	//rotatie in jurul axei Z
	auxMouseFunc(AUX_LEFTBUTTON, AUX_MOUSEDOWN, rot_z_up);
	auxMouseFunc(AUX_RIGHTBUTTON, AUX_MOUSEDOWN, rot_z_down);

	auxReshapeFunc(myReshape);
	auxMainLoop(display);
	return(0);
}
