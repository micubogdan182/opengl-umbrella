This draws an umbrella using OpenGL

The umbrella is drawn using cylinders which were reshaped.
It has lighting, shaders and you can freely rotate the umbrella on different axis.